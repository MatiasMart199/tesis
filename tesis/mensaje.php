<script>
    function verificar_mensaje(resultado) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        var response = JSON.parse(resultado);

        Toast.fire({
            type: response.success ? 'success' : 'error',
            title: response.message.replace(/_\/_/g, ' ')
        });
    }
</script>

